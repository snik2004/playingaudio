package com.snik2004.playingaudio;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    ImageButton imageButtonPlay;
    ImageButton imageButtonSkipPrevious;
    ImageButton imageButtonSkipNext;
    MediaPlayer mediaPlayer;
    boolean startStop = true;
    SeekBar volumeSeekBar;
    AudioManager audioManager;
    SeekBar playSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        playSeekBar.setMax(mediaPlayer.getDuration());
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                playSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                if (playSeekBar.getProgress()==mediaPlayer.getDuration()){
                    imageButtonPlay.setImageResource(R.drawable.ic_play);
                    startStop = true;
                    mediaPlayer.seekTo(0);
                    playSeekBar.setProgress(0);
                }
            }
        }, 0, 1000);


        playSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volumeSeekBar.setMax(maxVolume);
        volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("Progress changed: ", "" + progress);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        imageButtonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startStop) {
                    mediaPlayer.start();
                    imageButtonPlay.setImageResource(R.drawable.ic_pause);
                    startStop = false;
                } else {
                    mediaPlayer.pause();
                    imageButtonPlay.setImageResource(R.drawable.ic_play);
                    startStop = true;
                }
            }
        });
        volumeSeekBar.setProgress(maxVolume / 2);
        imageButtonSkipPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToStart();
            }
        });

        imageButtonSkipNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEnd();
            }
        });
    }

    public void goToStart() {
        playSeekBar.setProgress(0);
        mediaPlayer.seekTo(0);
    }

    public void goToEnd() {
        playSeekBar.setProgress(mediaPlayer.getDuration());
        mediaPlayer.seekTo(mediaPlayer.getDuration());
    }

    public void init() {
        mediaPlayer = MediaPlayer.create(this, R.raw.stuff);
        imageButtonPlay = findViewById(R.id.play_button);
        imageButtonSkipPrevious = findViewById(R.id.skip_previous);
        imageButtonSkipNext = findViewById(R.id.skip_next);
        volumeSeekBar = findViewById(R.id.volumeSeekBar);
        playSeekBar = findViewById(R.id.play_SeekBar);
    }
}
